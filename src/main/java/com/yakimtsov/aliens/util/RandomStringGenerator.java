package com.yakimtsov.aliens.util;

import java.util.Random;

public class RandomStringGenerator {
    private static final String CHAR_LIST =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int LENGTH = 45;

    public String generateRandomString() {
        StringBuilder randStr = new StringBuilder();
        for (int i = 0; i < LENGTH; i++) {
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }

    private int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt == 0) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

}

