package com.yakimtsov.aliens.connection;

/**
 * connection pool exception
 */
public class ConnectionPoolException extends Exception {

    public ConnectionPoolException(String message){
        super(message);
    }

    public ConnectionPoolException(){ }

    public ConnectionPoolException(Throwable th){
        super(th);
    }

    public ConnectionPoolException(String m, Throwable th){
        super(m,th);
    }
}
