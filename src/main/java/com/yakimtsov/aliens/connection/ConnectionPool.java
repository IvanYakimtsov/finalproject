package com.yakimtsov.aliens.connection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class realise connection pool pattern
 */
public final class ConnectionPool {
    private static Logger logger = LogManager.getLogger();

    /**
     * contains available
     */
    private BlockingQueue<ProxyConnection> connectionQueue;
    /*
     * check if instance created
     */
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);

    /**
     * configuration properties
     */
    private Properties info = new Properties();

    /**
     * pool size value
     */
    private int poolSize;

    /**
     * database url
     */
    private String url;


    /**
     * connection pool instance
     */
    private static final ConnectionPool INSTANCE = new ConnectionPool();


    /**
     * pool constructor
     */
    private ConnectionPool() {
        if (instanceCreated.get()) {
            logger.log(Level.FATAL, "Tried to clone connection pool with reflection api");
            throw new RuntimeException("Tried to clone connection pool with reflection api");
        }
        instanceCreated.set(true);
    }

    /**
     * this method init connection pool
     */
    public void initPool() {
        registerJDBCDriver();
        initPoolData();
        createConnections();
    }

    /**
     * this method establish connection with db and init pool data
     */
    private void initPoolData() {
        try {
            info.load(this.getClass().getClassLoader().getResourceAsStream("poolConfig.properties"));
            url = info.getProperty("url");
            poolSize = Integer.parseInt(info.getProperty("poolSize"));

        } catch (IOException e) {
            logger.log(Level.FATAL, "Error while init pool data");
            throw new RuntimeException();
        }
        connectionQueue = new LinkedBlockingQueue<>();
    }

    /**
     * this method creates avaliable connections
     */
    private void createConnections() {
        for (int index = 0; index < poolSize; index++) {
            try {
                Connection dbConnection = DriverManager.getConnection(url, info);
                ProxyConnection proxyConnection = new ProxyConnection(dbConnection);
                connectionQueue.put(proxyConnection);
            } catch (InterruptedException | SQLException e) {
                logger.catching(e);
                throw new RuntimeException("Hasn't found connection with database");
            }
        }
    }

    /**
     * this method register db driver
     */
    private void registerJDBCDriver() {
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        } catch (SQLException e) {
            logger.catching(e);
            throw new RuntimeException();
        }
    }

    /**
     * this method return connection pool instance
     *
     * @return connection pool instance
     */
    public static ConnectionPool getInstance() {
        return INSTANCE;
    }


    /**
     * This method returns proxyConnection
     *
     * @return proxyConnection returns avaliable connection
     * @throws ConnectionPoolException throws exception if thread will be interrupted
     * @see ProxyConnection
     * @see ConnectionPoolException
     */
    public ProxyConnection takeConnection() throws ConnectionPoolException {
        try {
            return connectionQueue.take();
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e);
        }
    }

    /**
     * This method returns proxyConnection in pool
     *
     * @param connection proxyConnection that must be returned in pool
     * @see ProxyConnection
     */
    void releaseConnection(ProxyConnection connection) {
        try {
            if (!connectionQueue.contains(connection)) {
                if (!connection.getAutoCommit()) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                }
                connectionQueue.put(connection);
            }

        } catch (InterruptedException | SQLException e) {
            logger.catching(e);
            try {
                Connection dbConnection = DriverManager.getConnection(url, info);
                ProxyConnection proxyConnection = new ProxyConnection(dbConnection);
                connectionQueue.put(proxyConnection);
                connection.connection.close();
            } catch (SQLException | InterruptedException e1) {
                logger.catching(e1);
            }
        }

    }


    /**
     *This method returns all connection in pool and close them
     */
    public void dispose() {
        for (int i = 0; i < poolSize; i++) {
            try {
                ProxyConnection proxyConnection = connectionQueue.take();
                if (!proxyConnection.getAutoCommit()) {
                    proxyConnection.commit();
                }
                proxyConnection.connection.close();
            } catch (InterruptedException | SQLException e) {
                logger.catching(e);
            }
        }

        unregisterDriver();

    }

    /**
     * this method deregister db driver
     */
    private void unregisterDriver() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == loader) {
                try {
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    logger.catching(ex);
                }
            }
        }
    }

}