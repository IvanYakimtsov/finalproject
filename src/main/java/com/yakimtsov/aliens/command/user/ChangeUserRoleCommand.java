package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.UserReceiver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This command change user role
 */
public class ChangeUserRoleCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with users
     *
     * @see UserReceiver
     */
    private UserReceiver receiver = new UserReceiver();

    /**
     * URI template
     */
    private static final String PATH = "/pages?command=show_user_workplace&user_name=%s";

    public ChangeUserRoleCommand() {
        router.setRoute(Router.RouteType.REDIRECT);
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String adminName = (String) session.getAttribute("user_name");
        String name = request.getParameter("user_name");
        String role = request.getParameter("role");
        router.setPagePath(String.format(PATH,name));
        try {
            receiver.changeUserRole(name,role,adminName);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }


    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
