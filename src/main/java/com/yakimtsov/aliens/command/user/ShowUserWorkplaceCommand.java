package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.UserReceiver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
/**
 * This command show information about user
 */
public class ShowUserWorkplaceCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with users
     *
     * @see UserReceiver
     */
    private UserReceiver receiver = new UserReceiver();
    /**
     * Receiver that works with bans
     *
     * @see BanReceiver
     */
    private BanReceiver banReceiver = new BanReceiver();

    public ShowUserWorkplaceCommand() {
        router.setRoute(Router.RouteType.FORWARD);
        router.setPagePath("/pages/userPage.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String userName = request.getParameter("user_name");
        try {
            Optional<User> user = receiver.findUserInfo(userName);
            if (user.isPresent()) {
                request.setAttribute("user", user.get());
                HttpSession session = request.getSession();
                Optional<Ban> optionalBan = banReceiver.findBanByUserName(user.get().getName());
                Locale locale = (Locale) session.getAttribute("locale");
                ResourceBundle rb = ResourceBundle.getBundle("content", locale);
                String message = rb.getString("banUntil");
                optionalBan.ifPresent(ban -> request.setAttribute("banMessage",
                        message + ban.getEndTime().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))));
            } else {
                router.setErrorCode(HttpServletResponse.SC_NOT_FOUND);
            }

        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }


    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
