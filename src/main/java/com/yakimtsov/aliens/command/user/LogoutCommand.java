package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * This command log out
 */
public class LogoutCommand implements Command {

    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    public LogoutCommand() {
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages/homePage.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) {
        request.getSession().invalidate();

    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
