package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.RegistrationHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * This command submit user registration
 */
public class SubmitUserCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    public SubmitUserCommand(){
        router.setRoute(Router.RouteType.FORWARD);
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String id = request.getParameter("recordId");

        try {
            Optional<User> user = RegistrationHolder.getInstance().registerUser(id);
            if(user.isPresent()){
                request.setAttribute("message","Пользователь " + user.get().getEmail()
                        + " успешно заригестрирован");
                request.getSession().setAttribute("user", user.get().getEmail());
                request.getSession().setAttribute("user_name", user.get().getName());
                request.getSession().setAttribute("role", user.get().getRole());
                router.setPagePath("/pages/notificationPage.jsp");
            } else {
                request.setAttribute("message","Заявка не найдена");
                router.setPagePath("/pages/notificationPage.jsp");
            }

        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
