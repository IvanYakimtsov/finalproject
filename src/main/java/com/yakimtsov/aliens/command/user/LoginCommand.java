package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.UserReceiver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * This command log in user
 */
public class LoginCommand implements Command {
    /**
     * Receiver that works with users
     *
     * @see UserReceiver
     */
    private UserReceiver receiver = new UserReceiver();
    /**
     * Receiver that works with bans
     *
     * @see BanReceiver
     */
    private BanReceiver banReceiver = new BanReceiver();

    /**
     * Receiver that works with applications
     *
     * @see ApplicationReceiver
     */
    private ApplicationReceiver applicationReceiver = new ApplicationReceiver();

    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();


    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String userName = request.getParameter("login");
        String password = request.getParameter("password");
        Optional<User> user;
        try {
            user = receiver.findUser(userName, password);
            if (user.isPresent()) {
                Optional<Ban> ban = banReceiver.findBanByUserName(user.get().getName());

                if (!ban.isPresent()) {
                    router.setRoute(Router.RouteType.REDIRECT);
                    request.getSession().setAttribute("user", user.get().getEmail());
                    request.getSession().setAttribute("user_name", user.get().getName());
                    request.getSession().setAttribute("role", user.get().getRole());
                    router.setPagePath("/pages/homePage.jsp");
                } else {
                    router.setRoute(Router.RouteType.FORWARD);
                    request.setAttribute("ban", ban.get());
                    String applicationMessage = applicationReceiver
                            .findApplicationStatus(ban.get().getId());
                    if (!applicationMessage.isEmpty()) {
                        request.setAttribute("message", applicationMessage);
                    }
                    router.setPagePath("/pages/banPage.jsp");
                }

            } else {
                HttpSession session = request.getSession();
                Locale locale = (Locale) session.getAttribute("locale");
                ResourceBundle rb = ResourceBundle.getBundle("content", locale);
                String message = rb.getString("invalidRegistration");
                request.setAttribute("errorMessage", message);
                router.setRoute(Router.RouteType.FORWARD);
                router.setPagePath("/pages/loginPage.jsp");
            }

        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
