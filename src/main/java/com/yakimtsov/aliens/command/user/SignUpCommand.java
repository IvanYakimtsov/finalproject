package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.RegistrationHolder;

import javax.servlet.http.HttpServletRequest;

/**
 * This command sign up new user
 */
public class SignUpCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    public SignUpCommand() {
        router.setRoute(Router.RouteType.FORWARD);
    }


    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String email = request.getParameter("login");
        String name = request.getParameter("user_name");
        String password = request.getParameter("password");
        String server = request.getServerName() + ":" + request.getServerPort();
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setPassword(password);
        try {
            RegistrationHolder.RegistrationStatus registrationStatus =
                    RegistrationHolder.getInstance().signUpUser(user, server);
            switch (registrationStatus) {
                case OK:
                    request.setAttribute("message", "Заявка выслана на email " + user.getEmail());
                    router.setPagePath("/pages/notificationPage.jsp");
                    break;
                case EMAIL_IS_USED:
                    request.setAttribute("errorMessage", "пользователь " + user.getEmail() + " уже существует");
                    router.setPagePath("/pages/registrationPage.jsp");
                    break;
                case NAME_IS_USED:
                    request.setAttribute("errorMessage", "пользователь " + user.getName() + " уже существует");
                    router.setPagePath("/pages/registrationPage.jsp");
                    break;
            }
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
