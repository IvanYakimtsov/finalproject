package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.receiver.ReceiverException;
import com.yakimtsov.aliens.receiver.UserReceiver;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * This command show all users
 */
public class ShowAllUsersCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with users
     *
     * @see UserReceiver
     */
    private UserReceiver receiver = new UserReceiver();

    public ShowAllUsersCommand() {
        router.setRoute(Router.RouteType.FORWARD);
        router.setPagePath("/pages/userPages.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        try {
            List<User> adminList = receiver.findUserByRole("admin");
            List<User> moderList = receiver.findUserByRole("moder");
            List<User> userList = receiver.findUserByRole("user");

            request.setAttribute("adminList", adminList);
            request.setAttribute("moderList", moderList);
            request.setAttribute("userList", userList);
        } catch (ReceiverException e) {
            throw new CommandException(e);

        }


    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
