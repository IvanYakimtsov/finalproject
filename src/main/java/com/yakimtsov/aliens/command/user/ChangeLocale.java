package com.yakimtsov.aliens.command.user;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.Router;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
/**
 * This command change locale
 */
public class ChangeLocale implements Command {


    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    public ChangeLocale() {
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages/homePage.jsp");
    }


    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String language = request.getParameter("locale");
        session.setAttribute("locale",new Locale(language));

    }


    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
