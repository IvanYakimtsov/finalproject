package com.yakimtsov.aliens.command;


/**
 * This class defines command exception
 */
public class CommandException extends Exception {

    public CommandException(String message){
        super(message);
    }

    public CommandException(){ }

    public CommandException(Throwable th){
        super(th);
    }

    public CommandException(String m, Throwable th){
        super(m,th);
    }
}
