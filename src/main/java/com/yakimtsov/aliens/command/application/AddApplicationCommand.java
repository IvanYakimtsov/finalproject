package com.yakimtsov.aliens.command.application;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This command adds new application
 */
public class AddApplicationCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with applications
     *
     * @see ApplicationReceiver
     */
    private ApplicationReceiver receiver = new ApplicationReceiver();

    public AddApplicationCommand() {
        router.setRoute(Router.RouteType.FORWARD);
        router.setPagePath("/pages/notificationPage.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String banId = request.getParameter("id");
        String text = request.getParameter("text");
        try {
            receiver.addApplication(banId, text);
            HttpSession session = request.getSession();
            Locale locale = (Locale) session.getAttribute("locale");
            ResourceBundle rb = ResourceBundle.getBundle("content", locale);
            String message = rb.getString("confirmApplication");
            request.setAttribute("message", message);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }


    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
