package com.yakimtsov.aliens.command.application;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Application;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * This command shows all applications
 */
public class ShowApplicationsCommand implements Command {
    /**
     * This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with applications
     * @see ApplicationReceiver
     */
    private ApplicationReceiver receiver = new ApplicationReceiver();

    public ShowApplicationsCommand(){
        router.setRoute(Router.RouteType.FORWARD);
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        try {
            HttpSession session = request.getSession();
            String role = (String) session.getAttribute("role");
            if(role != null && (role.equals("admin") || role.equals("moder")
                    || role.equals("root_admin"))) {
                List<Application> applications = receiver.findNotApprovedApplications();
                request.setAttribute("list", applications);
                router.setPagePath("/pages/applicationsPage.jsp");
            } else {
               router.setErrorCode(HttpServletResponse.SC_FORBIDDEN);
            }
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
