package com.yakimtsov.aliens.command.application;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;

/**
 * This command hide application
 */
public class HideApplicationCommand implements Command {
    /**
     * This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with applications
     * @see ApplicationReceiver
     */
    private ApplicationReceiver receiver = new ApplicationReceiver();
    public HideApplicationCommand(){
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages?command=show_applications");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String banId = request.getParameter("id");
        try {
            receiver.hideApplication(banId);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
