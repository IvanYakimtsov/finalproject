package com.yakimtsov.aliens.command;

import com.yakimtsov.aliens.command.alien.*;
import com.yakimtsov.aliens.command.application.AddApplicationCommand;
import com.yakimtsov.aliens.command.application.HideApplicationCommand;
import com.yakimtsov.aliens.command.application.ShowApplicationsCommand;
import com.yakimtsov.aliens.command.ban.BanUserCommand;
import com.yakimtsov.aliens.command.ban.DeleteUserBanCommand;
import com.yakimtsov.aliens.command.ban.FindUserBanCommand;
import com.yakimtsov.aliens.command.complain.AddComplainCommand;
import com.yakimtsov.aliens.command.complain.DeleteComplainCommand;
import com.yakimtsov.aliens.command.complain.ShowComplainsCommand;
import com.yakimtsov.aliens.command.user.*;

import java.util.Optional;

/**
 * This class produce commands
 */
public class CommandFactory {

    /**
     * This class defines all command
     */
    public enum CommandType {
        ADD_ALIEN(new AddAlienCommand()),
        DELETE_ALIEN(new DeleteAlienCommand()),
        FIND_ALIENS(new FindAliensCommand()),
        SHOW_ALIEN(new ShowAlienCommand()),
        UPDATE_ALIEN(new UpdateAlienCommand()),
        LOGIN(new LoginCommand()),
        SIGN_UP(new SignUpCommand()),
        SUBMIT_USER(new SubmitUserCommand()),
        EDIT_ALIEN(new EditAlienCommand()),
        SHOW_USER_WORKPLACE(new ShowUserWorkplaceCommand()),
        BAN_USER(new BanUserCommand()),
        DELETE_BAN(new DeleteUserBanCommand()),
        FIND_USER_BAN(new FindUserBanCommand()),
        CHANGE_USER_ROLE(new ChangeUserRoleCommand()),
        SHOW_ALL_USERS(new ShowAllUsersCommand()),
        ADD_COMPLAIN(new AddComplainCommand()),
        SHOW_COMPLAINS(new ShowComplainsCommand()),
        DELETE_COMPLAIN(new DeleteComplainCommand()),
        BAN_APPLICATION(new AddApplicationCommand()),
        SHOW_APPLICATIONS(new ShowApplicationsCommand()),
        HIDE_APPLICATION(new HideApplicationCommand()),
        CHANGE_LOCALE(new ChangeLocale()),
        LOGOUT(new LogoutCommand());

        private Command command;

        /**
         * This constructor receive new command
         *
         * @param command command
         */
        CommandType(Command command) {

            this.command = command;
        }

        /**
         * This method returns command
         *
         * @return command return some command
         */
        public Command getCommand() {
            return command;
        }

        /**
         * This method  check if enum contains command
         *
         * @param test possible command
         *
         * @return boolean value
         */
        public static boolean contains(String test) {
            for (CommandType element : CommandType.values()) {
                if (element.name().equals(test)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * This method creates command by string value
     *
     * @param commandText possible command
     *
     * @return optional optional that may contain command
     */
    public static Optional<Command> createCommand(String commandText) {
        Command command = null;
        if (commandText != null && CommandType.contains(commandText.toUpperCase())) {
            command = CommandType.valueOf(commandText.toUpperCase()).getCommand();
        }
        return Optional.ofNullable(command);
    }
}
