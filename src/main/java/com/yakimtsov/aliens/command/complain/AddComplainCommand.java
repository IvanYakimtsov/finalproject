package com.yakimtsov.aliens.command.complain;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ComplainReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * This command adds new Complain
 */
public class AddComplainCommand implements Command {
    /**
     * Receiver that works with complains
     *
     * @see ComplainReceiver
     */
    private ComplainReceiver receiver = new ComplainReceiver();

    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    public AddComplainCommand() {
        router.setRoute(Router.RouteType.FORWARD);
        router.setPagePath("/pages/notificationPage.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("user_name");
        String toUser = request.getParameter("to_user");
        String reason = request.getParameter("reason");

        try {
            receiver.addComplain(userName, toUser, reason);

            Locale locale = (Locale) session.getAttribute("locale");
            ResourceBundle rb = ResourceBundle.getBundle("content", locale);
            String message = rb.getString("confirmApplication");
            request.setAttribute("message", message);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
