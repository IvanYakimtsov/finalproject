package com.yakimtsov.aliens.command.complain;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Complain;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ComplainReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * This command show all complains
 */
public class ShowComplainsCommand implements Command {
    /**
     * Receiver that works with complains
     *
     * @see ComplainReceiver
     */
    private ComplainReceiver receiver = new ComplainReceiver();

    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    public ShowComplainsCommand(){
        router.setRoute(Router.RouteType.FORWARD);
    }



    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        try {
            HttpSession session = request.getSession();
            String role = (String) session.getAttribute("role");

            if(role != null && (role.equals("admin") || role.equals("moder")
                    || role.equals("root_admin"))) {
                List<Complain>  complains = receiver.findComplains();
                router.setPagePath("/pages/complainsPage.jsp");
                request.setAttribute("list", complains);
            } else {
                router.setErrorCode(HttpServletResponse.SC_FORBIDDEN);
            }

        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
