package com.yakimtsov.aliens.command.complain;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.ComplainReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;

public class DeleteComplainCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    /**
     * Receiver that works with complains
     *
     * @see ComplainReceiver
     */
    private ComplainReceiver receiver = new ComplainReceiver();
    public DeleteComplainCommand(){
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages?command=show_complains");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String complainId = request.getParameter("id");
        try {
            receiver.deleteComplain(complainId);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
