package com.yakimtsov.aliens.command;

import javax.servlet.http.HttpServletRequest;
/**
 * This class defines basic command interface
 */
public interface Command{
    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    void execute(HttpServletRequest request) throws CommandException;
    /**
     * This method returns next page
     *
     * @return router return next page
     */
    Router getRouter();
}
