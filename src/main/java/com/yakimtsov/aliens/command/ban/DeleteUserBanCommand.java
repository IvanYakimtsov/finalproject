package com.yakimtsov.aliens.command.ban;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;

/**
 * This command delete user ban
 */
public class DeleteUserBanCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    /**
     * URI template
     */
    private static final String DEFAULT_PAGE_PATH = "/pages?command=show_user_workplace&user_name=%s";



    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        BanReceiver receiver = new BanReceiver();
        String name = request.getParameter("name");

        String pagePath = request.getParameter("page_path");
        if(pagePath != null){
            router.setRoute(Router.RouteType.REDIRECT);
            router.setPagePath(pagePath);
        } else {
            router.setRoute(Router.RouteType.FORWARD);
            router.setPagePath(String.format(DEFAULT_PAGE_PATH,name));
        }
        try {
            receiver.deleteUserBan(name);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
