package com.yakimtsov.aliens.command.ban;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * This command find information about user ban
 */
public class FindUserBanCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();
    /**
     * Receiver that works with user bans
     *
     * @see BanReceiver
     */
    private BanReceiver receiver = new BanReceiver();


    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("user_name");
        if(userName != null){
            try {
                Optional<Ban> ban = receiver.findBanByUserName(userName);
                if(ban.isPresent()){
                    session.invalidate();
                }
            } catch (ReceiverException e) {
                throw new CommandException(e);
            }
        }


    }

    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
