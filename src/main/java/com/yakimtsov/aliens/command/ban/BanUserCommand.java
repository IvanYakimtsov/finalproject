package com.yakimtsov.aliens.command.ban;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This command bans user
 */
public class BanUserCommand implements Command {
    /**
     * This variable allows to return next page
     *
     * @see Router
     */
    private Router router = new Router();

    /**
     * Receiver that works with bans
     *
     * @see BanReceiver
     */
    private BanReceiver receiver = new BanReceiver();

    /**
     * URI template
     */
    private static final String PATH = "/pages?command=show_user_workplace&user_name=%s";

    public BanUserCommand() {
        router.setRoute(Router.RouteType.FORWARD);
    }


    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String name = request.getParameter("name");
        router.setPagePath(String.format(PATH,name));
        String time = request.getParameter("time");
        String reason = request.getParameter("reason");

        try {
            LocalDateTime endTime = receiver.banUser(name, reason, time);
            HttpSession session = request.getSession();
            Locale locale = (Locale) session.getAttribute("locale");
            ResourceBundle rb = ResourceBundle.getBundle("content", locale);
            String message = rb.getString("banUntil");
            request.setAttribute("banMessage", message + " " + endTime
                    .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }


    /**
     * This method returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
