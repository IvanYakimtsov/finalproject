package com.yakimtsov.aliens.command;

/**
 * This class allows to return next page
 */
public class Router {
    /**
     * This variable contains page pass
     */
    private String pagePath;
    /**
     * This variable contains information about how we should visit another page
     */
    private RouteType route;
    /**
     * This variable contains error code
     */
    private int errorCode;

    /**
     * This variable enum contains possible ways of visiting another page
     */
    public enum RouteType {
        FORWARD, REDIRECT
    }

    /**
     * This method set page path
     *
     * @param pagePath page path
     */
    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    /**
     * This method set way of visiting other page
     *
     * @param route way type
     */
    public void setRoute(RouteType route) {
        this.route = route;
    }

    /**
     * This method return page path
     *
     * @return page path
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * This method return route type
     *
     * @return route type
     */
    public RouteType getRoute() {
        return route;
    }


    /**
     * This method return error code
     *
     * @return error code
     */
    public int getErrorCode() {
        return errorCode;
    }


    /**
     * This method set error code
     *
     * @param errorCode error code
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
