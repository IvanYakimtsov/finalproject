package com.yakimtsov.aliens.command.alien;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This update existed alien
 */
public class UpdateAlienCommand implements Command {
    /**
     * This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();
//    private static final String UPLOAD_DIR = "uploads";
    private static final String PATH_TEMPLATE = "/pages?command=show_alien&alien_name=";

    public UpdateAlienCommand() {
        router.setRoute(Router.RouteType.REDIRECT);
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        AlienReceiver receiver = new AlienReceiver();
        int alienId = Integer.parseInt(request.getParameter("alien_id"));
        String alienName = request.getParameter("alien_name");
        String description = request.getParameter("description");
        Alien alien = new Alien();
        alien.setId(alienId);
        alien.setAlienName(alienName);
        alien.setAlienDescription(description);

//        String applicationPath = request.getServletContext().getRealPath("");
//        String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;
//        File fileSaveDir = new File(uploadFilePath);
//        if(!fileSaveDir.exists()){
//            fileSaveDir.mkdirs();
//        }
//
//        //TODO: update pictures
        try {
//            for(Part part : request.getParts()) {
//                if (part.getSubmittedFileName() != null) {
//                    part.write(uploadFilePath + File.separator + part.getSubmittedFileName());
//                    alien.setAlienPicture(part.getSubmittedFileName());
//                }
//            }

//            if (!receiver.findAlienByName(alienName).isPresent()) {
//
//            } else {
//                router.setRoute(Router.RouteType.FORWARD);
//                router.setPagePath("/pages/notificationPage.jsp");
//                HttpSession session = request.getSession();
//                Locale locale = (Locale) session.getAttribute("locale");
//                ResourceBundle rb = ResourceBundle.getBundle("content", locale);
//                String message = rb.getString("alreadyExist");
//                request.setAttribute("message", message);
//            }
            receiver.updateAlien(alien);
            router.setPagePath(PATH_TEMPLATE+URLEncoder.encode(alienName, "UTF-8"));
        } catch (IOException|ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
