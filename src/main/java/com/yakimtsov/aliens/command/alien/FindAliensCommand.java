package com.yakimtsov.aliens.command.alien;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 *This command find all aliens
 */
public class FindAliensCommand implements Command {
    /**
     *This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();

    public FindAliensCommand(){
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages/homePage.jsp");
    }
    /**
     * This method contains command logic
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        AlienReceiver receiver = new AlienReceiver();
        try {
            List<Alien> alienList = receiver.findAliensHeaders();
            request.setAttribute("list", alienList);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }

    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
