package com.yakimtsov.aliens.command.alien;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This command adds new alien
 */
public class AddAlienCommand implements Command {
    /**
     *This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();
    /**
     * Path template for redirected page
     */
    private static final String PATH_TEMPLATE = "/pages?command=show_alien&alien_name=";

    /**
     * Receiver that works with aliens
     * @see AlienReceiver
     */
    private AlienReceiver receiver = new AlienReceiver();


    /**
     * This method contains command logic
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        String name = request.getParameter("alien_name");
        String description = request.getParameter("description");
        //TODO: get picture
        String userName = (String) request.getSession().getAttribute("user_name");
        try {
            if (!receiver.findAlienByName(name).isPresent()) {
                receiver.addAlien(name, description, null, userName);
                router.setPagePath(PATH_TEMPLATE + URLEncoder.encode(name, "UTF-8"));
                router.setRoute(Router.RouteType.REDIRECT);
            } else {
                router.setRoute(Router.RouteType.FORWARD);
                router.setPagePath("/pages/notificationPage.jsp");
                HttpSession session = request.getSession();
                Locale locale = (Locale) session.getAttribute("locale");
                ResourceBundle rb = ResourceBundle.getBundle("content", locale);
                String message = rb.getString("alreadyExist");
                request.setAttribute("message", message);
            }

        } catch (ReceiverException | UnsupportedEncodingException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }

}
