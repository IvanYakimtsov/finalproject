package com.yakimtsov.aliens.command.alien;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;

/**
 * This command delete alien
 */
public class DeleteAlienCommand implements Command {

    /**
     *This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();

    public DeleteAlienCommand(){
        router.setRoute(Router.RouteType.REDIRECT);
        router.setPagePath("/pages/homePage.jsp");
    }

    /**
     * This method contains command logic
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        AlienReceiver receiver = new AlienReceiver();
        String alienName = request.getParameter("alien_name");
        try {
            receiver.deleteAlien(alienName);
        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }

    /**
     * This returns next page
     *
     * @return router return next page
     */
    @Override
    public Router getRouter() {
        return router;
    }
}
