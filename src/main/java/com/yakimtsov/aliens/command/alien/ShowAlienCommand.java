package com.yakimtsov.aliens.command.alien;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.Router;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * This command find information about alien
 */
public class ShowAlienCommand implements Command {
    /**
     * This variable allows to return next page
     * @see Router
     */
    private Router router = new Router();

    public ShowAlienCommand() {
        router.setRoute(Router.RouteType.FORWARD);
        router.setPagePath("/pages/alienPage.jsp");
    }

    /**
     * This method contains command logic
     *
     * @param request servlet request that must be processed
     */
    @Override
    public void execute(HttpServletRequest request) throws CommandException {
        AlienReceiver receiver = new AlienReceiver();

        String alienName = request.getParameter("alien_name");

        try {
            Optional<Alien> alien = receiver.findAlienByName(alienName);
            if (alien.isPresent()) {
                request.setAttribute("entity", alien.get());
            } else {
                router.setErrorCode(HttpServletResponse.SC_NOT_FOUND);
            }

        } catch (ReceiverException e) {
            throw new CommandException(e);
        }
    }


    /**
     * This returns next page
     *
     * @return router return next page
     */
    public Router getRouter() {
        return router;
    }
}
