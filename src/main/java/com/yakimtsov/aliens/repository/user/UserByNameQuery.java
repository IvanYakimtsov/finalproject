package com.yakimtsov.aliens.repository.user;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class UserByNameQuery implements SqlSpecification {
    private static final String SQL = "select name from users where name = ?";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public UserByNameQuery(String name){
        parameters.add(name);
        fields.add("name");
    }
    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}
