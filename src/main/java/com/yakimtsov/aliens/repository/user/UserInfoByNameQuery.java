package com.yakimtsov.aliens.repository.user;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class UserInfoByNameQuery implements SqlSpecification {
    private static final String SQL = "select name,password,email,avatar,creation_date,role" +
            " from users where name = ?";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public UserInfoByNameQuery(String name){
        parameters.add(name);
        fields.add("name");
        fields.add("password");
        fields.add("email");
        fields.add("avatar");
        fields.add("creation_date");
        fields.add("role");
    }
    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}
