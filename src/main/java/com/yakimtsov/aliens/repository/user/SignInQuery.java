package com.yakimtsov.aliens.repository.user;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class SignInQuery implements SqlSpecification {
    private final static String SQL = "select name,email,role from users where email = ?  and " +
            "password = sha1(concat(?,(select password_salt from users where email = ? limit 1))) ";
    private List<Object> parameters;

    private ArrayList<String> fields = new ArrayList<>();

    public SignInQuery(String email, String password){
        parameters = new ArrayList<>();
        parameters.add(email);
        parameters.add(password);
        parameters.add(email);

        fields.add("name");
        fields.add("role");
        fields.add("surname");
    }

    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed){
        return fields.contains(filed);
    }
}
