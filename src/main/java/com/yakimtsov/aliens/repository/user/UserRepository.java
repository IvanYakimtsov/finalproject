package com.yakimtsov.aliens.repository.user;


import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.connection.ConnectionPoolException;
import com.yakimtsov.aliens.connection.ProxyConnection;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.repository.Repository;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;
import com.yakimtsov.aliens.util.RandomStringGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository implements Repository<User> {
    private final String ADD_USER_SQL = "INSERT INTO `users`\n" +
            "(`name`,`email`,`password`,`password_salt`,`avatar`,`creation_date`," +
            "`role`) VALUES (?,?,sha1(?),?,?,now(),\"user\");";

    private final String DELETE_USER_SQL = "DELETE FROM `users` WHERE `name` = ?;";

    private final String UPDATE_USER_SQL = "UPDATE `users` SET`email` = ?," +
            "`password` = ?,`avatar` = ?,`role` = ? WHERE `name` = ?;";

    private RandomStringGenerator generator = new RandomStringGenerator();

    @Override
    public void add(User user) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            String salt = generator.generateRandomString();
            PreparedStatement statement = connection.prepareStatement(ADD_USER_SQL);
            statement.setObject(1, user.getName());
            statement.setObject(2, user.getEmail());
            statement.setObject(3, user.getPassword() + salt);
            statement.setObject(4, salt);
            statement.setObject(5, user.getAvatar());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void delete(User user) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_USER_SQL);
            statement.setObject(1, user.getName());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    @Override
    public void update(User entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL);
            statement.setObject(1, entity.getEmail());
            statement.setObject(2, entity.getPassword());
            statement.setObject(3, entity.getAvatar());
            statement.setObject(4, entity.getRole());
            statement.setObject(5, entity.getName());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<User> query(SqlSpecification specification) throws RepositoryException {
        List<User> users = new ArrayList<>();
        String sql = specification.getSql();
        try (ProxyConnection dbConnection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = dbConnection.prepareStatement(sql);

            for (int index = 0; index < specification.getParameters().size(); index++) {
                statement.setObject(index + 1, specification.getParameters().get(index));
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
               User user = createUser(resultSet,specification);
                users.add(user);
            }
            statement.close();

            return users;
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    private User createUser(ResultSet resultSet, SqlSpecification specification) throws SQLException {
        User user = new User();

        if(specification.isFieldRequired("name")){
            String name = resultSet.getString("name");
            user.setName(name);
        }

        if(specification.isFieldRequired("email")){
            String email = resultSet.getString("email");
            user.setEmail(email);
        }

        if(specification.isFieldRequired("password")){
            String password = resultSet.getString("password");
            user.setPassword(password);
        }

        if(specification.isFieldRequired("password_salt")){
            String passwordSalt = resultSet.getString("password_salt");
            user.setPasswordSalt(passwordSalt);
        }

        if(specification.isFieldRequired("avatar")){
            String avatar = resultSet.getString("avatar");
            user.setAvatar(avatar);
        }


        if(specification.isFieldRequired("creation_date")){
            Date creationDate = resultSet.getDate("creation_date");
            user.setCreationDate(creationDate);
        }

        if(specification.isFieldRequired("role")){
            String role = resultSet.getString("role");
            user.setRole(role);
        }

        return user;
    }

}
