package com.yakimtsov.aliens.repository.complain;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class AllComplainsQuery implements SqlSpecification {
    private static final String SQL = "select id,to_user, " +
            "from_user, reason from complains";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public AllComplainsQuery(){
        fields.add("id");
        fields.add("to_user");
        fields.add("from_user");
        fields.add("reason");
    }
    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}
