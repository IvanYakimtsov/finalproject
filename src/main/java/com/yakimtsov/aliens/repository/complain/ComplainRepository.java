package com.yakimtsov.aliens.repository.complain;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.connection.ConnectionPoolException;
import com.yakimtsov.aliens.connection.ProxyConnection;
import com.yakimtsov.aliens.entity.Complain;
import com.yakimtsov.aliens.repository.Repository;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ComplainRepository implements Repository<Complain> {
    private final static String ADD_COMPLAIN_SQL = "INSERT INTO `complains`" +
            "(`to_user`,`from_user`,`reason`)VALUES(?,?,?)";
    private final static String DELETE_COMPLAIN_SQL = "DELETE FROM `complains` WHERE `id` = ?";

    private final static String UPDATE_COMPLAIN_SQL = "UPDATE `complains` SET `to_user` = ?, " +
            "`from_user` = ?, `reason` = ? WHERE `id` = ?";

    @Override
    public void add(Complain entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(ADD_COMPLAIN_SQL);
            statement.setObject(1, entity.getToUser());
            statement.setObject(2, entity.getFromUser());
            statement.setObject(3, entity.getReason());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void delete(Complain entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_COMPLAIN_SQL);
            statement.setObject(1, entity.getId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void update(Complain entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_COMPLAIN_SQL);
            statement.setObject(1, entity.getToUser());
            statement.setObject(2, entity.getFromUser());
            statement.setObject(3, entity.getReason());
            statement.setObject(4, entity.getId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<Complain> query(SqlSpecification specification) throws RepositoryException {
        List<Complain> complains = new ArrayList<>();
        String sql = specification.getSql();
        try (ProxyConnection dbConnection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = dbConnection.prepareStatement(sql);

            for (int index = 0; index < specification.getParameters().size(); index++) {
                statement.setObject(index + 1, specification.getParameters().get(index));
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Complain complain = createComplain(resultSet, specification);
                complains.add(complain);
            }
            statement.close();

            return complains;
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    private Complain createComplain(ResultSet resultSet, SqlSpecification specification) throws SQLException {

        Complain complain = new Complain();

        if (specification.isFieldRequired("id")) {
            int id = resultSet.getInt("id");
            complain.setId(id);
        }

        if (specification.isFieldRequired("to_user")) {
            String userName = resultSet.getString("to_user");
            complain.setToUser(userName);
        }

        if (specification.isFieldRequired("from_user")) {
            String userName = resultSet.getString("from_user");
            complain.setFromUser(userName);
        }

        if (specification.isFieldRequired("reason")) {
            String reason = resultSet.getString("reason");
            complain.setReason(reason);
        }

        return complain;
    }
}
