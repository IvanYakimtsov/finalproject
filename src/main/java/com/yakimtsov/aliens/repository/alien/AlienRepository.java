package com.yakimtsov.aliens.repository.alien;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.connection.ConnectionPoolException;
import com.yakimtsov.aliens.connection.ProxyConnection;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.repository.Repository;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;
import lombok.Generated;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AlienRepository implements Repository<Alien> {
    private final static String ADD_ALIEN_SQL = "INSERT INTO aliens(alien_name, alien_description," +
            "alien_picture, user_name) VALUES (?,?,?,?)";
    private final static String DELETE_ALIEN_SQL = "DELETE FROM aliens WHERE alien_name=?";
    private final static String UPDATE_ALIEN_SQL = "UPDATE aliens SET alien_name = ?, alien_description = ?," +
            " alien_picture =? WHERE alien_id=?";

    @Override
    public void add(Alien entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(ADD_ALIEN_SQL);
            statement.setObject(1, entity.getAlienName());
            statement.setObject(2, entity.getAlienDescription());
            statement.setObject(3, entity.getAlienPicture());
            statement.setObject(4, entity.getUserName());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void delete(Alien entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_ALIEN_SQL);
            statement.setObject(1, entity.getAlienName());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void update(Alien entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_ALIEN_SQL);
            statement.setObject(1, entity.getAlienName());
            statement.setObject(2, entity.getAlienDescription());
            statement.setObject(3, entity.getAlienPicture());
            statement.setObject(4, entity.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<Alien> query(SqlSpecification specification) throws RepositoryException {
        List<Alien> aliens = new ArrayList<>();
        String sql = specification.getSql();
        try (ProxyConnection dbConnection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = dbConnection.prepareStatement(sql);

            for (int index = 0; index < specification.getParameters().size(); index++) {
                statement.setObject(index + 1, specification.getParameters().get(index));
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Alien alien = createAlien(resultSet,specification);
                aliens.add(alien);
            }
            statement.close();

            return aliens;
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    private Alien createAlien(ResultSet resultSet, SqlSpecification specification) throws SQLException {
        Alien alien = new Alien();

        if(specification.isFieldRequired("alien_id")){
            int alienId = resultSet.getInt("alien_id");
            alien.setId(alienId);
        }

        if(specification.isFieldRequired("alien_name")){
            String alienName = resultSet.getString("alien_name");
            alien.setAlienName(alienName);
        }

        if(specification.isFieldRequired("alien_description")){
            String alienDescription = resultSet.getString("alien_description");
            alien.setAlienDescription(alienDescription);
        }

        if(specification.isFieldRequired("alien_picture")){
            String alienPicture = resultSet.getString("alien_picture");
            alien.setAlienPicture(alienPicture);
        }

        if(specification.isFieldRequired("user_name")){
            String userName = resultSet.getString("user_name");
            alien.setUserName(userName);
        }
        return alien;
    }
}
