package com.yakimtsov.aliens.repository.alien;


import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class AlienByNameQuery implements SqlSpecification {
    private final static String SQL = "select alien_id,alien_name,alien_description," +
            "alien_picture,user_name from aliens where alien_name = ?";
    private List<Object> parameters;
    private ArrayList<String> fields = new ArrayList<>();

    public AlienByNameQuery(String name){
        parameters = new ArrayList<>();
        parameters.add(name);

        fields.add("alien_id");
        fields.add("alien_name");
        fields.add("alien_description");
        fields.add("alien_picture");
        fields.add("user_name");
    }

    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}