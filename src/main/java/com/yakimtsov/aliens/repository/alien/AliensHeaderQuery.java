package com.yakimtsov.aliens.repository.alien;


import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class AliensHeaderQuery implements SqlSpecification {
    private final static String SQL = "select alien_name,user_name from aliens";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public AliensHeaderQuery(){
        fields.add("alien_name");
        fields.add("user_name");
    }

    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}