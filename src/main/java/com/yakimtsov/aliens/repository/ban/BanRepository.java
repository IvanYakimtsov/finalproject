package com.yakimtsov.aliens.repository.ban;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.connection.ConnectionPoolException;
import com.yakimtsov.aliens.connection.ProxyConnection;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.repository.Repository;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BanRepository implements Repository<Ban> {
    private final static String ADD_BAN_SQL = "INSERT INTO `bans`\n" +
            "(`user_name`,`end_time`,`reason`)\n" +
            "VALUES (?,?,?)";

    private final static String DELETE_BAN_SQL = "DELETE FROM `bans` WHERE ban_id = ?";

    private final static String UPDATE_BAN_SQL = "UPDATE `bans`" +
            "SET `user_name` = ?,`end_time` = ?, `reason` = ?" +
            "WHERE `ban_id` = ?";
    @Override
    public void add(Ban entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(ADD_BAN_SQL);
            statement.setObject(1, entity.getUserName());
            statement.setObject(2, entity.getEndTime());
            statement.setObject(3, entity.getReason());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    @Override
    public void delete(Ban entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_BAN_SQL);
            statement.setObject(1, entity.getId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void update(Ban entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_BAN_SQL);
            statement.setObject(1, entity.getUserName());
            statement.setObject(2, entity.getEndTime());
            statement.setObject(3, entity.getReason());
            statement.setObject(4, entity.getId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    @Override
    public List<Ban> query(SqlSpecification specification) throws RepositoryException {
        List<Ban> bans = new ArrayList<>();
        String sql = specification.getSql();
        try (ProxyConnection dbConnection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = dbConnection.prepareStatement(sql);

            for (int index = 0; index < specification.getParameters().size(); index++) {
                statement.setObject(index + 1, specification.getParameters().get(index));
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Ban ban = createBan(resultSet,specification);
                bans.add(ban);
            }
            statement.close();

            return bans;
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    private Ban createBan(ResultSet resultSet, SqlSpecification specification) throws SQLException {
        Ban ban = new Ban();

        if(specification.isFieldRequired("ban_id")){
            int banId = resultSet.getInt("ban_id");
            ban.setId(banId);
        }

        if(specification.isFieldRequired("user_name")){
            String userName = resultSet.getString("user_name");
            ban.setUserName(userName);
        }

//        if(specification.isFieldRequired("start_time")){
//            LocalDateTime startTime = resultSet.getTimestamp("start_time").toLocalDateTime();
//            ban.setStartTime(startTime);
//        }

        if(specification.isFieldRequired("end_time")){
            LocalDateTime endTime = resultSet.getTimestamp("end_time").toLocalDateTime();
            ban.setEndTime(endTime);
        }

        if(specification.isFieldRequired("reason")){
            String reason = resultSet.getString("reason");
            ban.setReason(reason);
        }
        return ban;
    }
}
