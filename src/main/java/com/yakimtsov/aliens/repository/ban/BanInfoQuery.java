package com.yakimtsov.aliens.repository.ban;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class BanInfoQuery implements SqlSpecification {
    private static final String SQL = "select ban_id,user_name, " +
            " end_time, reason from bans where user_name = ?";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public BanInfoQuery(String name){
        parameters.add(name);

        fields.add("ban_id");
        fields.add("user_name");
        fields.add("end_time");
        fields.add("reason");
    }

    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}
