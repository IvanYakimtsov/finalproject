package com.yakimtsov.aliens.repository;

import java.util.List;

public interface Repository<T>{
    void add(T entity) throws RepositoryException;
    void delete(T entity) throws RepositoryException;
    void update(T entity) throws RepositoryException;
    List<T> query(SqlSpecification specification) throws RepositoryException;
}
