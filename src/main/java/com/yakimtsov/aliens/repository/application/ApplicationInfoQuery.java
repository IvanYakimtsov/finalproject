package com.yakimtsov.aliens.repository.application;

import com.yakimtsov.aliens.repository.SqlSpecification;

import java.util.ArrayList;
import java.util.List;

public class ApplicationInfoQuery implements SqlSpecification {
    private static final String SQL = "select ban_id, text, approved from applications where ban_id = ?";
    private List<Object> parameters = new ArrayList<>();
    private ArrayList<String> fields = new ArrayList<>();

    public ApplicationInfoQuery(int id){
        parameters.add(id);

        fields.add("ban_id");
        fields.add("text");
        fields.add("approved");
    }
    @Override
    public String getSql() {
        return SQL;
    }

    @Override
    public List<Object> getParameters() {
        return parameters;
    }

    @Override
    public boolean isFieldRequired(String filed) {
        return fields.contains(filed);
    }
}
