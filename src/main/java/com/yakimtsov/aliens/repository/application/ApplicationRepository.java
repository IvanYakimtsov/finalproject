package com.yakimtsov.aliens.repository.application;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.connection.ConnectionPoolException;
import com.yakimtsov.aliens.connection.ProxyConnection;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.entity.Application;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.repository.Repository;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ApplicationRepository implements Repository<Application> {
    private static final String ADD_APPLICATION_SQL = "INSERT INTO `applications`" +
            "(`ban_id`, `text`, `approved`) VALUES (?,?,0)";
    private static final String DELETE_APPLICATION_SQL = "DELETE FROM `applications`" +
            "WHERE ban_id = ?";
    private static final String UPDATE_APPLICATION_SQL = "UPDATE  `applications`" +
            "SET `approved` = ? WHERE `ban_id` = ?";

    @Override
    public void add(Application entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(ADD_APPLICATION_SQL);
            statement.setObject(1, entity.getBanId());
            statement.setObject(2, entity.getText());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void delete(Application entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_APPLICATION_SQL);
            statement.setObject(1, entity.getBanId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void update(Application entity) throws RepositoryException {
        try (Connection connection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_APPLICATION_SQL);
            statement.setObject(1, entity.isApproved());
            statement.setObject(2, entity.getBanId());
            statement.execute();
            statement.close();
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }

    }

    @Override
    public List<Application> query(SqlSpecification specification) throws RepositoryException {
        List<Application> applications = new ArrayList<>();
        String sql = specification.getSql();
        try (ProxyConnection dbConnection = ConnectionPool.getInstance().takeConnection()) {
            PreparedStatement statement = dbConnection.prepareStatement(sql);

            for (int index = 0; index < specification.getParameters().size(); index++) {
                statement.setObject(index + 1, specification.getParameters().get(index));
            }

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Application application = createApplication(resultSet,specification);
                applications.add(application);
            }
            statement.close();

            return applications;
        } catch (SQLException | ConnectionPoolException e) {
            throw new RepositoryException(e);
        }
    }

    private Application createApplication(ResultSet resultSet, SqlSpecification specification) throws SQLException {
        Application application = new Application();

        if(specification.isFieldRequired("ban_id")){
            int banId = resultSet.getInt("ban_id");
            application.setBanId(banId);
        }

        if(specification.isFieldRequired("text")){
            String text = resultSet.getString("text");
            application.setText(text);
        }

        if(specification.isFieldRequired("approved")){
            Boolean isApproved = resultSet.getBoolean("approved");
            application.setApproved(isApproved);
        }


        return application;
    }
}
