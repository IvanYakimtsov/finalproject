package com.yakimtsov.aliens.filter;

import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.alien.FindAliensCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/pages/homePage.jsp"}, filterName = "homePageFilter")
public class HomePageFilter implements Filter {
    private static Logger logger = LogManager.getLogger();

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
//
//        servletRequest.setCharacterEncoding("UTF-8");
        //System.out.println("+");
        try {
            new FindAliensCommand().execute((HttpServletRequest) servletRequest);
        } catch (CommandException e) {
            throw new ServletException(e);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
