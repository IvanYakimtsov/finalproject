package com.yakimtsov.aliens.filter;


import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.ban.FindUserBanCommand;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"}, filterName = "banFilter")
public class BanFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        FindUserBanCommand command = new FindUserBanCommand();
        try {
            command.execute((HttpServletRequest) servletRequest);
        } catch (CommandException e) {
            throw new ServletException(e);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
