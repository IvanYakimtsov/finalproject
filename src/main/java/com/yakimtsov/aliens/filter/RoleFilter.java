package com.yakimtsov.aliens.filter;

import com.yakimtsov.aliens.entity.Alien;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/editAlienPage.jsp","/complainsPage.jsp","/applicationsPage.jsp"}, filterName = "roleFilter")
public class RoleFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
//
//        servletRequest.setCharacterEncoding("UTF-8");
        HttpSession session = ((HttpServletRequest)servletRequest).getSession();
        String user = (String) session.getAttribute("user");
        String role = (String) session.getAttribute("role");

        if(user != null && role != null && (role.equals("admin") || role.equals("moder"))){
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse)servletResponse).sendRedirect("/pages/homePage.jsp");
        }


    }

    @Override
    public void destroy() {

    }
}
