package com.yakimtsov.aliens.entity;



public class Alien {
    private int id;
    private String alienName;
    private String alienDescription;
    private String alienPicture;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlienName() {
        return alienName;
    }

    public void setAlienName(String alienName) {
        this.alienName = alienName;
    }

    public String getAlienDescription() {
        return alienDescription;
    }

    public void setAlienDescription(String alienDescription) {
        this.alienDescription = alienDescription;
    }

    public String getAlienPicture() {
        return alienPicture;
    }

    public void setAlienPicture(String alienPicture) {
        this.alienPicture = alienPicture;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
