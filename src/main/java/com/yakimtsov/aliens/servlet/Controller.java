package com.yakimtsov.aliens.servlet;

import com.yakimtsov.aliens.command.Command;
import com.yakimtsov.aliens.command.CommandException;
import com.yakimtsov.aliens.command.CommandFactory;
import com.yakimtsov.aliens.command.Router;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/pages")
@MultipartConfig(
        maxFileSize         = 1024 * 1024 * 10 // 10 MB
)
public class Controller extends HttpServlet {
    private static Logger logger = LogManager.getLogger();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(request, response);
    }

    private void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandValue = request.getParameter("command");
        System.out.println(commandValue);
        Optional<Command> command = CommandFactory.createCommand(commandValue);
        if (command.isPresent()) {
            try {
                command.get().execute(request);
                Router router = command.get().getRouter();
                if (router != null) {
                    if (router.getErrorCode() != 0) {
                        response.sendError(router.getErrorCode());
                        router.setErrorCode(0);
                    } else {
                        if (router.getPagePath() != null && router.getRoute().equals(Router.RouteType.FORWARD)) {
                            request.getRequestDispatcher(router.getPagePath()).forward(request, response);
                        } else if (router.getRoute().equals(Router.RouteType.REDIRECT)) {
                            response.sendRedirect(router.getPagePath());

                        }
                    }
                }
            } catch (CommandException e) {
                throw new ServletException(e);
            }


        } else {
            response.sendRedirect("/pages/homePage.jsp");
        }
    }
}