package com.yakimtsov.aliens.servlet;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Locale;

@WebListener
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        Locale locale = (Locale) session.getAttribute("locale");
        session.setAttribute("locale",new Locale("ru"));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }
}
