package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.SqlSpecification;
import com.yakimtsov.aliens.repository.ban.BanInfoByIdQuery;
import com.yakimtsov.aliens.repository.ban.BanInfoQuery;
import com.yakimtsov.aliens.repository.ban.BanRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;


public class BanReceiver {
    private BanRepository repository = new BanRepository();

    public LocalDateTime banUser(String name, String reason, String banHours) throws ReceiverException {
        if(reason.contains("<script>") || name.contains("<script>")){
            throw new ReceiverException("script injection alert");
        }
        //TODO: replace on hour
        if(new Scanner(banHours).hasNextInt()){
            LocalDateTime endTime = LocalDateTime.now().plusMinutes(Long.valueOf(banHours));

            if (findBanByUserName(name).isPresent()) {
                throw new ReceiverException("user already banned");
            }
            Ban ban = new Ban();
            ban.setUserName(name);
            ban.setReason(reason);
            ban.setEndTime(endTime);
            try {
                repository.add(ban);
                return endTime;
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
        } else {
            throw new ReceiverException("invalid ban hours");
        }

    }

    public Optional<Ban> findBanByUserName(String userName) throws ReceiverException {
        try {
           return findBan(new BanInfoQuery(userName));
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    Optional<Ban> findBanById(int id) throws ReceiverException {
        try {
            return findBan(new BanInfoByIdQuery(id));
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    private Optional<Ban> findBan(SqlSpecification specification) throws RepositoryException {
        List<Ban> bans = repository.query(specification);
        if (!bans.isEmpty()) {
            Ban ban = bans.get(0);
            LocalDateTime endTime = ban.getEndTime();
            if (LocalDateTime.now().isAfter(endTime)) {
                repository.delete(ban);
                return Optional.empty();
            }
            ban.setEndTimeStr(ban.getEndTime()
                    .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
            return Optional.of(ban);
        } else {
            return Optional.empty();
        }
    }


    public void deleteUserBan(String name) throws ReceiverException {
        try {
            Optional<Ban> optionalBan = findBanByUserName(name);
            if (optionalBan.isPresent()) {
                repository.delete(optionalBan.get());
            }

        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }
}
