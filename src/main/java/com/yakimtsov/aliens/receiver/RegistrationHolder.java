package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.user.UserRepository;
import com.yakimtsov.aliens.repository.user.UserByEmailQuery;
import com.yakimtsov.aliens.repository.user.UserByNameQuery;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public final class RegistrationHolder {
    private static Logger logger = LogManager.getLogger();
    private ReentrantLock signUpLock = new ReentrantLock();
    private ReentrantLock submitLock = new ReentrantLock();
    private static final RegistrationHolder INSTANCE = new RegistrationHolder();
    private ConcurrentHashMap<Long, User> usersApplications = new ConcurrentHashMap<>();
    private UserRepository userRepository = new UserRepository();

    public enum RegistrationStatus {
        OK, EMAIL_IS_USED, NAME_IS_USED, WEAK_PASSWORD;
    }


    private final static String URN = "/pages?command=submit_user&recordId=";

    private RegistrationHolder() {
    }

    public static RegistrationHolder getInstance() {
        return INSTANCE;
    }


    public RegistrationStatus signUpUser(User user, String hostname) throws ReceiverException {
        if(user.getName().contains("<script>")){
            throw new ReceiverException("script injection alert");
        }
        try {
            signUpLock.lock();
            user.setRole("user");
            try {
                List<User> users = userRepository.query(new UserByEmailQuery(user.getEmail()));
                if (!users.isEmpty()) {
                    return RegistrationStatus.EMAIL_IS_USED;
                }

                users = userRepository.query(new UserByNameQuery(user.getName()));
                if (!users.isEmpty()) {
                    return RegistrationStatus.NAME_IS_USED;
                }

                for (User entity : usersApplications.values()) {
                    if (entity.getEmail().equals(user.getEmail())) {
                        return RegistrationStatus.EMAIL_IS_USED;
                    }

                    if (entity.getName().equals(user.getName())) {
                        return RegistrationStatus.NAME_IS_USED;
                    }
                }
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
            long id = UUID.randomUUID().getMostSignificantBits();
            usersApplications.put(id, user);


            startMailThread(user.getEmail(), "HTTP://" + hostname + URN + id);
            startTimer(id);

            return RegistrationStatus.OK;
        } finally {
            signUpLock.unlock();
        }

    }

    private void startMailThread(String email, String Uri) {
        new Thread(() -> {
            try {
                sendEmail(email, Uri);
            } catch (IOException | MessagingException e) {
                logger.catching(e);
            }
        }).start();
    }

    private void startTimer(long id) {
        new Timer(true).schedule(new TimerTask() {
            @Override
            public void run() {
                usersApplications.remove(id);
            }
        }, TimeUnit.MINUTES.toMillis(1));
        //TODO: set hour delay
    }

    private void sendEmail(String email, String registrationUrl) throws IOException, MessagingException {
        Properties configProperties = new Properties();

        configProperties.load(this.getClass().getClassLoader().getResourceAsStream("mail.properties"));

        String userName = configProperties.getProperty("mail.user.name");
        String password = configProperties.getProperty("mail.user.password");
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(configProperties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);


        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(email)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject("Registration");
        msg.setSentDate(new Date());
        // msg.setText(registrationUrl);
        logger.log(Level.DEBUG, registrationUrl);
        msg.setContent("You registration URN \n" + "<a href=" + registrationUrl + ">" + registrationUrl + "</a>", "text/html");

        // sends the e-mail
        Transport.send(msg);

    }

    public Optional<User> registerUser(String recordIndex) throws ReceiverException {
        try {
            submitLock.lock();
            if (new Scanner(recordIndex).hasNextLong()) {
                long id = Long.parseLong(recordIndex);
                Optional<User> user = Optional.ofNullable(usersApplications.get(id));
                if (user.isPresent()) {
                    try {
                        userRepository.add(user.get());
                        usersApplications.remove(id);
                    } catch (RepositoryException e) {
                        throw new ReceiverException(e);
                    }
                }
                return user;
            } else {
                throw new ReceiverException("invalid id");
            }

        } finally {
            submitLock.unlock();
        }

    }

}
