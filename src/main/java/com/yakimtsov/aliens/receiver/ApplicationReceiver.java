package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.Application;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.application.ApplicationInfoQuery;
import com.yakimtsov.aliens.repository.application.ApplicationRepository;
import com.yakimtsov.aliens.repository.application.NotApprovedApplicationsQuery;
import com.yakimtsov.aliens.repository.ban.BanInfoByIdQuery;
import com.yakimtsov.aliens.repository.ban.BanRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class ApplicationReceiver {
    private ApplicationRepository repository = new ApplicationRepository();
    private BanReceiver banReceiver = new BanReceiver();

    public void addApplication(String banId, String text) throws ReceiverException {
        if(text.contains("<script>")){
            throw new ReceiverException("script injection alert");
        }
        Application application = new Application();
        if (new Scanner(banId).hasNextInt()) {
            application.setBanId(Integer.parseInt(banId));
            application.setText(text);
            try {
                repository.add(application);
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
        } else {
            throw new ReceiverException("invalid id");
        }

    }

    public String findApplicationStatus(int banId) throws ReceiverException {
        try {
            List<Application> applications = repository.query(new ApplicationInfoQuery(banId));
            if (!applications.isEmpty()) {

                if (applications.get(0).isApproved()) {
                    return "ваша жалоба отклонена";
                } else {
                    return "ваша жалоба отправленна";
                }
            } else {
                return "";
            }
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public List<Application> findNotApprovedApplications() throws ReceiverException {
        try {
            List<Application> applications = repository.query(new NotApprovedApplicationsQuery());
            List<Application> actualApplications = new ArrayList<>();
            for (Application application : applications) {
                Optional<Ban> optionalBan = banReceiver.findBanById(application.getBanId());
                if(optionalBan.isPresent()){
                    application.setBan(optionalBan.get());
                    actualApplications.add(application);
                } else {
                    repository.delete(application);
                }

            }
            return actualApplications;
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public void hideApplication(String banId) throws ReceiverException {
        if (new Scanner(banId).hasNextInt()) {
            int id = Integer.valueOf(banId);
            Application application = new Application();
            application.setBanId(id);
            application.setApproved(true);
            try {
                repository.update(application);
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
        } else {
            throw new ReceiverException("invalid id");
        }

    }
}
