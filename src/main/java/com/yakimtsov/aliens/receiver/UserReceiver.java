package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.user.SignInQuery;
import com.yakimtsov.aliens.repository.user.UserByRoleQuery;
import com.yakimtsov.aliens.repository.user.UserInfoByNameQuery;
import com.yakimtsov.aliens.repository.user.UserRepository;

import java.util.List;
import java.util.Optional;

public class UserReceiver {
    private UserRepository userRepository = new UserRepository();

    public Optional<User> findUser(String username, String password) throws ReceiverException {

        try {
            List<User> users = userRepository.query(new SignInQuery(username,password));
            if(!users.isEmpty()){
                return Optional.of(users.get(0));
            } else {
                return Optional.empty();
            }
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }

    }

    public Optional<User> findUserInfo(String username) throws ReceiverException {
        try {
            List<User> users = userRepository.query(new UserInfoByNameQuery(username));
            if(!users.isEmpty()){
                return Optional.of(users.get(0));
            } else {
                return Optional.empty();
            }
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }

    }

    public void changeUserRole(String name, String role, String adminName) throws ReceiverException {
        try {
            User user = null;
            User admin = null;
            List<User> users = userRepository.query(new UserInfoByNameQuery(name));
            if(!users.isEmpty()){
                user = users.get(0);
            }
            users = userRepository.query(new UserInfoByNameQuery(adminName));
            if(!users.isEmpty()){
                admin = users.get(0);
            }
            if(user != null || admin != null){
                if("admin".equals(admin.getRole()) || "root_admin".equals(admin.getRole())){
                    user.setRole(role);
                    userRepository.update(user);
                } else {
                    throw new ReceiverException("invalid access rights");
                }
            } else {
                throw new ReceiverException("invalid user or admin");
            }

        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }


    public List<User> findUserByRole(String role) throws ReceiverException {
        try {
            return userRepository.query(new UserByRoleQuery(role));
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }

    }
}
