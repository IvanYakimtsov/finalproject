package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.alien.AlienByNameQuery;
import com.yakimtsov.aliens.repository.alien.AlienRepository;
import com.yakimtsov.aliens.repository.alien.AliensHeaderQuery;

import java.util.List;
import java.util.Optional;

public class AlienReceiver {
    private AlienRepository repository = new AlienRepository();

    public List<Alien> findAliensHeaders() throws ReceiverException {
        try {
            return repository.query(new AliensHeaderQuery());
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public Optional<Alien> findAlienByName(String name) throws ReceiverException {
        try {
            List<Alien> aliens = repository.query(new AlienByNameQuery(name));
            if(!aliens.isEmpty()){
                return Optional.of(aliens.get(0));
            } else {
                return Optional.empty();
            }
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public void updateAlien(Alien alien) throws ReceiverException {
        try {
            if(alien.getAlienName().contains("<script>") || alien.getAlienDescription().contains("<script>")){
                throw new ReceiverException("script injection alert");
            }
            repository.update(alien);
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public void deleteAlien(String name) throws ReceiverException {
        Alien alien = new Alien();
        alien.setAlienName(name);
        try {
            repository.delete(alien);
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public void addAlien(String name, String description, String picture, String username) throws ReceiverException {
        try {
            if(name.contains("<script>") || description.contains("<script>")){
                throw new ReceiverException("script injection alert");
            }
            Alien alien = new Alien();
            alien.setAlienName(name);
            alien.setAlienDescription(description);
            alien.setAlienPicture(picture);
            alien.setUserName(username);
            repository.add(alien);
        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

}
