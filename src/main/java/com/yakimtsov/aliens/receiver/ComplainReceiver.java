package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.entity.Complain;
import com.yakimtsov.aliens.repository.RepositoryException;
import com.yakimtsov.aliens.repository.complain.AllComplainsQuery;
import com.yakimtsov.aliens.repository.complain.ComplainRepository;

import java.util.List;
import java.util.Scanner;

public class ComplainReceiver {
    private ComplainRepository repository = new ComplainRepository();

    public void addComplain(String userName, String toUser, String reason) throws ReceiverException {
        if (reason.contains("<script>")) {
            throw new ReceiverException("script injection alert");
        }
        if (userName != null) {
            try {
                Complain complain = new Complain();
                complain.setFromUser(userName);
                complain.setToUser(toUser);
                complain.setReason(reason);
                repository.add(complain);
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
        } else {
            throw new ReceiverException("unauthorized user cannot complain");
        }
    }

    public List<Complain> findComplains() throws ReceiverException {
        try {
            return repository.query(new AllComplainsQuery());

        } catch (RepositoryException e) {
            throw new ReceiverException(e);
        }
    }

    public void deleteComplain(String complainId) throws ReceiverException {
        if (new Scanner(complainId).hasNextInt()) {
            int id = Integer.valueOf(complainId);
            Complain complain = new Complain();
            complain.setId(id);
            try {
                repository.delete(complain);
            } catch (RepositoryException e) {
                throw new ReceiverException(e);
            }
        } else {
            throw new ReceiverException("invalid id");
        }
    }

}
