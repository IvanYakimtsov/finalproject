package com.yakimtsov.aliens.receiver;

public class ReceiverException extends Exception {

    public ReceiverException(String message){
        super(message);
    }

    public ReceiverException(){ }

    public ReceiverException(Throwable th){
        super(th);
    }

    public ReceiverException(String m, Throwable th){
        super(m,th);
    }
}
