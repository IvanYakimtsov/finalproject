<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="language" var="language"/>
<fmt:message bundle="${content}" key="enter" var="enter"/>
<fmt:message bundle="${content}" key="exit" var="exit"/>
<fmt:message bundle="${content}" key="control" var="control"/>
<fmt:message bundle="${content}" key="create" var="create"/>
<fmt:message bundle="${content}" key="posted" var="posted"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="edit" var="edit"/>
<!DOCTYPE html>
<html lang="en">
<c:if test="${empty entity}">
    <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
</c:if>
<head>

    <meta charset="utf-8">
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alien page</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="${pageContext.request.contextPath}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/pages/homePage.jsp">Zeta talk</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <c:choose>
                    <c:when test="${empty sessionScope.user_name && empty sessionScope.role}">
                        <li class="nav-item">
                            <a class="nav-link" href="${pageContext.request.contextPath}/pages/loginPage.jsp">${enter}</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${sessionScope.user_name}">${sessionScope.user_name}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="${pageContext.request.contextPath}/pages?command=logout">${exit}</a>
                        </li>
                        <c:if test="${sessionScope.role == 'admin' || sessionScope.role == 'moder'}">
                            <li class="nav-item">
                                <a class="nav-link" href="${pageContext.request.contextPath}/pages/adminPage.jsp">${control}</a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="${pageContext.request.contextPath}/pages/addAlienPage.jsp">${create}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${language}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/pages?command=change_locale&locale=en">English</a>
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/pages?command=change_locale&locale=ru">Русский</a>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- Page Header -->
<header class="masthead" style="background-image: url('${entity.alienPicture}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1><c:out value="${entity.alienName}"/></h1>
                    <span class="meta">${posted}
                <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.userName}"><c:out value="${entity.userName}"/></a>
                        </span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <c:out value="${entity.alienDescription}"/>
            </div>
        </div>
    </div>
</article>

<hr>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                    <c:if test="${entity.userName == sessionScope.user_name || sessionScope.role == 'admin'
                    || sessionScope.role == 'moder'}">

                        <form action="${pageContext.request.contextPath}/pages" method="post">
                            <input type="hidden" name="command" value="edit_alien">
                            <input type="hidden" name="alien_name" value="${entity.alienName}">
                                <%--<input type="hidden" name="alien_id" value="${entity.id}">--%>

                            <input type="submit" class="btn btn-primary" name="edit" value="${edit}">
                        </form>
                        <br>
                        <form action="${pageContext.request.contextPath}/pages/homePage.jsp">
                            <input class="btn btn-primary" type="submit" value="${home}"/>
                        </form>
                    </c:if>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Your Website 2018</p>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for this template -->
<script src="${pageContext.request.contextPath}/js/clean-blog.min.js"></script>

</body>

</html>
