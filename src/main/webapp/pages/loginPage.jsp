<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="loginWelcome" var="loginWelcome"/>
<fmt:message bundle="${content}" key="loginOption" var="loginOption"/>
<fmt:message bundle="${content}" key="stayLogin" var="stayLogin"/>
<fmt:message bundle="${content}" key="enter" var="enter"/>
<fmt:message bundle="${content}" key="registate" var="registate"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<html>
<head>
    <title>Login</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">


<div class="container">
    <div class="card bg-light">
        <article class="card-body mx-auto" style="max-width: 400px;">
            <h4 class="card-title mt-3 text-center">${loginWelcome}</h4>
            <p class="text-center">${loginOption}</p>
            <c:if test="${not empty errorMessage}">
                <div class="alert alert-danger form-group">
                    <a class="close" data-dismiss="alert" href="#">×</a>${errorMessage}
                </div>
            </c:if>
            <form action="${pageContext.request.contextPath}/pages" method="post">
                <input type="hidden" name="command" value="login">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="login" class="form-control" placeholder="Email address" type="email">
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control" name="password" placeholder="password" type="password">
                </div> <!-- form-group// -->
                <div class="form-check-inline form-group">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="">${stayLogin}
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> ${enter}  </button>
                </div> <!-- form-group// -->

            </form>
            <hr>
            <form action="${pageContext.request.contextPath}/pages/registrationPage.jsp">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> ${registate} </button>
                </div> <!-- form-group// -->
            </form>
            <form action="${pageContext.request.contextPath}/pages/homePage.jsp">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> ${home} </button>
                </div> <!-- form-group// -->
            </form>
        </article>
    </div> <!-- card.// -->

</div>
<!--container end.//-->
</body>
</html>
