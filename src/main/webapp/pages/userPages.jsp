<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="admins" var="admins"/>
<fmt:message bundle="${content}" key="users" var="users"/>
<fmt:message bundle="${content}" key="moders" var="moders"/>
<html>
<head>
    <title>Title</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
    <c:if test="${sessionScope.role != 'admin' && sessionScope.role != 'moder'}">
        <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
    </c:if>
</head>
<body>
${admins}
<br/>
<table>
    <c:forEach items="${adminList}" var="item">
        <tr>
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.name}">${item.name}</a>
        </tr>

        <br/>
    </c:forEach>
</table>

<br/>
${moders}
<br/>
<table>
    <c:forEach items="${moderList}" var="item">
        <tr>
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.name}">${item.name}</a>
        </tr>

        <br/>
    </c:forEach>
</table>
<br/>
${users}
<br/>
<table>
    <c:forEach items="${userList}" var="item">
        <tr>
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.name}">${item.name}</a>
        </tr>

        <br/>
    </c:forEach>
</table>
<br/>
<form action="${pageContext.request.contextPath}/index.jsp">
    <input type="submit" value="${home}"/>
</form>
</body>
</html>
