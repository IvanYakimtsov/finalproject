<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="language" var="language"/>
<fmt:message bundle="${content}" key="fromUser" var="fromUser"/>
<fmt:message bundle="${content}" key="reason" var="reason"/>
<fmt:message bundle="${content}" key="banReason" var="banReason"/>
<fmt:message bundle="${content}" key="banUntil" var="banUntil"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="hide" var="hide"/>
<fmt:message bundle="${content}" key="unban" var="unban"/>
<fmt:message bundle="${content}" key="complains" var="complains"/>
<html>
<head>
    <title>Applications</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
    <c:if test="${sessionScope.role != 'admin' && sessionScope.role != 'moder'}">
        <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
    </c:if>
</head>
<body>
${complains}:
<br/>
<table>
    <c:forEach items="${list}" var="item">
        <tr>
            ${fromUser}
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.ban.userName}"><c:out value="${item.ban.userName}"/></a>
            <br/>
            ${reason}
            <c:out value="${item.text}"/>
            <br/>
            ${banReason}
            <c:out value="${item.ban.reason}"/>
            <br/>
            ${banUntil}
            <c:out value="${item.ban.endTimeStr}"/>
            <br/>
            <form action="${pageContext.request.contextPath}/pages" method="post">
                <input type="hidden" name="command" value="hide_application">
                <input type="hidden" name="id" value="${item.banId}">
                <input type="submit" name="edit" value="${hide}">
            </form>
            <form action="${pageContext.request.contextPath}/pages" method="post">
                <input type="hidden" name="command" value="delete_ban">
                <input type="hidden" name="name" value="${item.ban.userName}">
                <input type="hidden" name="page_path" value="/pages?command=show_applications">
                <input type="submit" name="edit" value="${unban}">
            </form>
        </tr>

        <br/>
    </c:forEach>
</table>

<form action="${pageContext.request.contextPath}/index.jsp">
    <input type="submit" value="${home}"/>
</form>
</body>
</html>
