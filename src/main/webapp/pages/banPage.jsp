<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="language" var="language"/>
<fmt:message bundle="${content}" key="banUntil" var="banUntil"/>
<fmt:message bundle="${content}" key="reason" var="reason"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="banComplain" var="banComplain"/>
<html>
<head>
    <title>Title</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
</head>
<body>
${banUntil} ${ban.endTimeStr}
<br>
${reason}: <c:out value="${ban.reason}"/>
<br/>
<img src="${pageContext.request.contextPath}/img/ban.gif" alt="ban picture">
<br/>
<br/>

<c:choose>
    <c:when test="${empty message}">
        <form action="${pageContext.request.contextPath}/pages">
            <input type="hidden" name="command" value="ban_application">
            <input type="hidden" name="id" value="${ban.id}">
            <textarea rows="10" cols="50" name="text" required></textarea>
            <br/>
            <input type="submit" value="${banComplain}"/>
        </form>
    </c:when>
    <c:otherwise>
       <c:out value="${message}"/>;
    </c:otherwise>
</c:choose>
<br/>
<form action="${pageContext.request.contextPath}/pages/homePage.jsp">
    <input type="submit" value="${home}"/>
</form>
</body>
</html>
