<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="youProfile" var="youProfile"/>
<fmt:message bundle="${content}" key="userProfile" var="userProfile"/>
<fmt:message bundle="${content}" key="joining" var="joining"/>
<fmt:message bundle="${content}" key="userName" var="userName"/>
<fmt:message bundle="${content}" key="userEmail" var="userEmail"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="makeAdmin" var="makeAdmin"/>
<fmt:message bundle="${content}" key="makeModer" var="makeModer"/>
<fmt:message bundle="${content}" key="makeUser" var="makeUser"/>
<fmt:message bundle="${content}" key="complain" var="complain"/>
<fmt:message bundle="${content}" key="unban" var="unban"/>
<fmt:message bundle="${content}" key="ban" var="ban"/>
<html>
<head>
    <title>Profile</title>
    <link href="${pageContext.request.contextPath}/css/profile.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico"/>
</head>
<body>
<c:if test="${empty user}">
    <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
</c:if>

<div id="profile" class="container">
    <div class="row">


        <div class="col-md-7 ">

            <div class="panel panel-default">


                <c:choose>
                    <c:when test="${user.name == sessionScope.user_name}">
                        <div class="panel-heading"><h4>${youProfile}</h4></div>
                    </c:when>
                    <c:otherwise>
                        <div class="panel-heading"><h4>${userProfile}</h4></div>
                    </c:otherwise>
                </c:choose>
                <div class="panel-body">

                    <div class="box box-info">

                        <div class="box-body">
                            <div class="col-sm-6">

                                <c:choose>
                                    <c:when test="${empty user.avatar}">
                                        <div align="center"><img alt="User Pic"
                                                                 src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg"
                                                                 class="img-circle img-responsive">
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div align="center"><img alt="User Pic"
                                                                 src="${user.avatar}"
                                                                 class="img-circle img-responsive">
                                        </div>
                                    </c:otherwise>
                                </c:choose>



                                <br>

                                <!-- /input-group -->
                            </div>
                            <div class="col-sm-6">
                                <h4 style="color:#00b1b1;">${user.name}</h4></span>
                                <span><p>${user.role}</p></span>
                            </div>
                            <div class="clearfix"></div>
                            <hr style="margin:5px 0 5px 0;">


                            <div class="col-sm-5 col-xs-6 tital ">${userName}</div>
                            <div class="col-sm-7 col-xs-6 "><c:out value="${user.name}"/></div>
                            <div class="clearfix"></div>
                            <div class="bot-border"></div>

                            <div class="col-sm-5 col-xs-6 tital ">${userEmail}</div>
                            <div class="col-sm-7"> <c:out value="${user.email}"/></div>

                            <div class="clearfix"></div>
                            <div class="bot-border"></div>

                            <div class="col-sm-5 col-xs-6 tital ">${joining}</div>
                            <div class="col-sm-7">${user.creationDate}</div>

                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </div>


                </div>
            </div>
        </div>

    </div>
    <c:if test="${not empty sessionScope.user_name && user.name != sessionScope.user_name}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="add_complain">
    <input type="hidden" name="to_user" value=${user.name}>
        Пожаловаться:
    <textarea id="complain" class="form-control" rows="10" placeholder="reason" name="reason" required></textarea>
    <br/>
    <input type="submit" class="btn btn-primary" name="warn" value="${complain}">
    </form>
        <hr>
    </c:if>
    <c:if test="${not empty banMessage}">
        <div id="ban_message" class="alert alert-danger form-group">
            <a class="close" data-dismiss="alert" href="#">×</a>${banMessage}
        </div>
    </c:if>
    <c:if test="${((sessionScope.role == 'moder' && user.role == 'user')|| sessionScope.role == 'admin'
    && user.role != 'root_admin')
    && sessionScope.user_name != user.name && empty banMessage}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="ban_user">
    <input type="hidden" name="name" value="${user.name}">
        Забанить:
    <input id="ban_time" type="text" class="form-control form-control-plaintext" name="time" placeholder="ban time in hours" required>
    <br/>
    <textarea id="ban" class="form-control" rows="10" placeholder="reason" name="reason" required></textarea>
    <br/>
    <input type="submit" class="btn btn-primary" name="edit" value="${ban}">
    </form>
        <hr>
    </c:if>

    <c:if test="${(sessionScope.role == 'moder' || sessionScope.role == 'admin')
    && sessionScope.user_name != user.name && not empty banMessage}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="delete_ban">
    <input type="hidden" name="name" value="${user.name}">
    <input type="submit" class="btn btn-primary" name="edit" value="${unban}">
    </form>
        <hr>
    </c:if>


    <c:if test="${sessionScope.role == 'admin' && sessionScope.user_name != user.name}">

    <c:if test="${user.role == 'admin' || user.role == 'moder'}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="change_user_role">
    <input type="hidden" name="role" value="user">
    <input type="hidden" name="user_name" value=${user.name}>
    <input type="submit" class="btn btn-primary" name="edit" value="${makeUser}">
    </form>
    </c:if>


    <c:if test="${user.role == 'admin' || user.role == 'user'}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="change_user_role">
    <input type="hidden" name="role" value="moder">
    <input type="hidden" name="user_name" value=${user.name}>
    <input type="submit" class="btn btn-primary" name="edit" value="${makeModer}">
    </form>
    </c:if>

    <c:if test="${user.role == 'moder' || user.role == 'user'}">
    <form action="${pageContext.request.contextPath}/pages" method="post">
    <input type="hidden" name="command" value="change_user_role">
    <input type="hidden" name="role" value="admin">
    <input type="hidden" name="user_name" value=${user.name}>
    <input type="submit" class="btn btn-primary" name="edit" value="${makeAdmin}">
    </form>
    </c:if>
    </c:if>

    <form action="${pageContext.request.contextPath}/index.jsp">
    <input type="submit" class="btn btn-primary" value="${home}"/>
    </form>
</div>
</body>
</html>
