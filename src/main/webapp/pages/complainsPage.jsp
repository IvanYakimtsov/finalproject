<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="fromUser" var="fromUser"/>
<fmt:message bundle="${content}" key="complains" var="complains"/>
<fmt:message bundle="${content}" key="reason" var="reason"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="toUser" var="toUser"/>
<fmt:message bundle="${content}" key="delete" var="delete"/>
<html>
<head>
    <title>Title</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
    <c:if test="${sessionScope.role != 'admin' && sessionScope.role != 'moder'}">
        <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
    </c:if>
</head>
<body>
${complains}:
<br/>
<table>
    <c:forEach items="${list}" var="item">
        <tr>
            ${toUser}:
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.toUser}"><c:out value="${item.toUser}"/></a>
            <br/>
            ${fromUser}:
            <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.fromUser}"><c:out value="${item.fromUser}"/></a>
            <br/>
            ${reason}:
            <c:out value="${item.reason}"/>
            <br/>
            <form action="${pageContext.request.contextPath}/pages" method="post">
                <input type="hidden" name="command" value="delete_complain">
                <input type="hidden" name="id" value="${item.id}">
                <input type="submit" name="edit" value="${delete}">
            </form>
        </tr>

        <br/>
    </c:forEach>
</table>

<form action="${pageContext.request.contextPath}/index.jsp">
    <input type="submit" value="${home}"/>
</form>
</body>
</html>
