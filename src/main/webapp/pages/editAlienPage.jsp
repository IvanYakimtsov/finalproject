<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="posted" var="posted"/>
<fmt:message bundle="${content}" key="save" var="save"/>
<fmt:message bundle="${content}" key="delete" var="delete"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<html lang="en">
<c:if test="${empty entity}">
    <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
</c:if>
<head>

    <meta charset="utf-8">
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">
        #alien_name{color: white;
            background-color: black;
        font-size: 30px;
        font-weight: 300;
        font-style: italic;
        font-family: Lora,'Times New Roman',serif;
        margin-left: -2%}
        .btn{
            margin-bottom: 2%;
        }
        .upload{
            margin-top: 2%;
            font-size: 20px;
            font-weight: 300;
            font-style: italic;
            font-family: Lora,'Times New Roman',serif;
        }

        .center{
            margin-left: 25%;
        }
    </style>

    <title>Edit page</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="${pageContext.request.contextPath}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css/clean-blog.min.css" rel="stylesheet">

</head>

<body>
<!-- Page Header -->
<%--<form action="${pageContext.request.contextPath}/upload" class="upload" method="post" enctype='multipart/form-data'>--%>
    <%--<input type="file" value="upload_alien" class="form-control-file border" size="50">--%>
    <%--<br/>--%>
    <%--<input type="submit" name="command" value="Upload File"/>--%>
<%--</form>--%>
<form action="${pageContext.request.contextPath}/pages" method="post" >
    <input type="hidden" name="command" value="update_alien">
    <input type="hidden" name="alien_id" value="${entity.id}">

<header class="masthead" style="background-image: url('${entity.alienPicture}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1> <input id="alien_name" type="text" class="form-control form-control-plaintext"  value="${entity.alienName}" placeholder="alien name" name="alien_name" required></h1>
                    <span class="meta">${posted}
                <a href="${pageContext.request.contextPath}/pages?command=show_user_workplace&user_name=${item.userName}">${entity.userName}</a>
                        </span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <textarea rows="50" class="form-control" placeholder="alien description" name="description">${entity.alienDescription}</textarea>
            </div>
        </div>
    </div>
</article>

<hr>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">

                    <br>
                </ul>
                <%--<p class="copyright text-muted">Copyright &copy; Your Website 2018</p>--%>
            </div>
        </div>
    </div>
</footer>
    <input class="center btn btn-primary" type="submit" name="update" value="${save}">

<!-- Bootstrap core JavaScript -->
<script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for this template -->
<script src="${pageContext.request.contextPath}/js/clean-blog.min.js"></script>
</form>

<form action="pages" method="post">
    <input type="hidden" name="command" value="delete_alien">
    <input type="hidden" name="alien_name" value="${entity.alienName}">
    <input class="center btn btn-primary" type="submit" name="delete" value="${delete}">
</form>
<form action="${pageContext.request.contextPath}/index.jsp">
    <input class="center btn btn-primary" type="submit" value="${home}"/>
</form>

</body>

</html>