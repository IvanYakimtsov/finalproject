<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="content" var="content" scope="session"/>
<fmt:message bundle="${content}" key="home" var="home"/>
<fmt:message bundle="${content}" key="usersList" var="usersList"/>
<fmt:message bundle="${content}" key="complainsList" var="complainsList"/>
<fmt:message bundle="${content}" key="banComplainsList" var="banComplainsList"/>
<html>
<head>
    <title>Control panel</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/img/alien.ico" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <c:if test="${sessionScope.role != 'admin' && sessionScope.role != 'moder'}">
        <c:redirect url="${pageContext.request.contextPath}/pages/homePage.jsp"/>
    </c:if>
</head>
<body>
<form action="${pageContext.request.contextPath}/pages" method="get">
    <input type="hidden" name="command" value="show_all_users">
    <input type="submit" class="btn btn-primary" name="edit" value="${usersList}">
</form>

<form action="${pageContext.request.contextPath}/pages" method="get">
    <input type="hidden" name="command" value="show_complains">
    <input type="submit" class="btn btn-primary" value="${complainsList}">
</form>
<form action="${pageContext.request.contextPath}/pages" method="get">
    <input type="hidden" name="command" value="show_applications">
    <input type="submit" class="btn btn-primary" value="${banComplainsList}">
</form>

<form action="${pageContext.request.contextPath}/pages/homePage.jsp">
    <input type="submit" class="btn btn-primary" value="${home}"/>
</form>
</body>
</html>
