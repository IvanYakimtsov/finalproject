package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Ban;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class BanReceiverTest {
    BanReceiver banReceiver = new BanReceiver();
    @BeforeClass
    public void setUp(){
        ConnectionPool.getInstance().initPool();
        try {
            banReceiver.banUser("test","test","200");
        } catch (ReceiverException e) {
            fail();
        }
    }
    @AfterClass
    public void dispose(){
        try {
            banReceiver.deleteUserBan("test");
            ConnectionPool.getInstance().dispose();
        } catch (ReceiverException e) {
            fail();
        }

    }

    @Test
    public void findBanByName(){
        try {
            Optional<Ban> ban = banReceiver.findBanByUserName("test");
            assertTrue(ban.isPresent());
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void findBanById(){
        try {
            Optional<Ban> ban = banReceiver.findBanByUserName("test");
            Optional<Ban> test = banReceiver.findBanById(ban.get().getId());
            assertTrue(test.isPresent());
        } catch (ReceiverException e) {
            fail();
        }
    }

}
