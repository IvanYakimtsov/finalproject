package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Application;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.receiver.ApplicationReceiver;
import com.yakimtsov.aliens.receiver.BanReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class ApplicationReceiverTest {
    ApplicationReceiver receiver = new ApplicationReceiver();
    Application application = new Application();
    BanReceiver banReceiver = new BanReceiver();
    Ban ban;
    @BeforeClass
    public void setUp(){
        ConnectionPool.getInstance().initPool();
        try {
            banReceiver.banUser("test","test","200");
            Optional<Ban> ban = banReceiver.findBanByUserName("test");
            this.ban = ban.get();
            receiver.addApplication(String.valueOf(this.ban.getId()),"test");
        } catch (ReceiverException e) {
            fail();
        }
    }
    @AfterClass
    public void dispose(){
        try {
            banReceiver.deleteUserBan("test");
            ConnectionPool.getInstance().dispose();
        } catch (ReceiverException e) {
            fail();
        }

    }
    @Test
    public void findApplicationStatus(){
        try {
            String status = receiver.findApplicationStatus(ban.getId());
            assertEquals(status,"ваша жалоба отправленна");
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void findNotApprovedApplication(){
        try {
            List<Application> applications = receiver.findNotApprovedApplications();
            assertEquals(applications.size(),1);
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void hideApplication(){
        try {
            receiver.hideApplication(String.valueOf(ban.getId()));
            List<Application> applications = receiver.findNotApprovedApplications();
            assertEquals(applications.size(),0);
        } catch (ReceiverException e) {
            fail();
        }
    }
}
