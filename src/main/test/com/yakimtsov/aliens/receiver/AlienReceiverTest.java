package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.receiver.AlienReceiver;
import com.yakimtsov.aliens.receiver.ReceiverException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.testng.Assert.*;

public class AlienReceiverTest {
    AlienReceiver receiver = new AlienReceiver();
    Alien alien = new Alien();
    @BeforeClass
    public void setUp(){
        ConnectionPool.getInstance().initPool();
        try {
            alien.setAlienName("test");
            receiver.addAlien("test","test","test","test");
        } catch (ReceiverException e) {
            fail();
        }
    }
    @AfterClass
    public void dispose(){
        ConnectionPool.getInstance().dispose();
    }
    @Test
    public void findAlienHeader(){
        try {
            List<Alien> test = receiver.findAliensHeaders();
            assertEquals(test.size(),7);
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void findAlienByName(){
        try {
            Optional<Alien> test = receiver.findAlienByName("Test");
            assertTrue(test.isPresent());
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void updateAlien(){
        try {
            Optional<Alien> test = receiver.findAlienByName("Test");
            if(test.isPresent()){
                test.get().setAlienName("Check");
                receiver.updateAlien(test.get());
                Optional<Alien> testName = receiver.findAlienByName("Check");
                assertTrue(testName.isPresent());
            } else {
                fail();
            }

        } catch (ReceiverException e) {
            fail();
        }
    }


    @Test
    public void deleteAlien(){
        try {
            receiver.deleteAlien("Check");
            Optional<Alien> testName = receiver.findAlienByName("Check");
            assertFalse(testName.isPresent());
        } catch (ReceiverException e) {
            fail();
        }
    }
}
