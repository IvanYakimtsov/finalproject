package com.yakimtsov.aliens.receiver;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Ban;
import com.yakimtsov.aliens.entity.Complain;
import com.yakimtsov.aliens.entity.User;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class UserReceiverTest {
    UserReceiver receiver = new UserReceiver();

    @BeforeClass
    public void setUp() {
        ConnectionPool.getInstance().initPool();
    }

    @AfterClass
    public void dispose() {
        ConnectionPool.getInstance().dispose();
    }

    @Test
    public void findUser() {
        try {
            Optional<User> user = receiver.findUser("ivan.yakimtspv@gmail.com","qwerty12");
            assertTrue(user.isPresent());
        } catch (ReceiverException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void findUserInfo(){
        try {
            Optional<User> user = receiver.findUserInfo("Ivan");
            assertTrue(user.isPresent());
        } catch (ReceiverException e) {
            fail();
        }
    }

    @Test
    public void userByRole(){
        try {
            List<User> users = receiver.findUserByRole("admin");
            assertEquals(users.size(),2);
        } catch (ReceiverException e) {
            fail();
        }
    }

}
