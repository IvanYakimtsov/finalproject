package com.yakimtsov.aliens.repository;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.repository.alien.AlienRepository;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.fail;

public class AlienRepositoryTest {
    AlienRepository repository = new AlienRepository();
    Alien alien = new Alien();

    @BeforeClass
    public void setUp() {
        ConnectionPool.getInstance().initPool();
        alien.setAlienName("test");
        alien.setAlienDescription("test");
        alien.setUserName("Ivan");
    }

    @AfterClass
    public void dispose() {
        ConnectionPool.getInstance().dispose();
    }

    @Test
    public void add(){
        try {
            repository.add(alien);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void update(){
        try {
            alien.setAlienDescription("dsfdsfsdf");
            repository.update(alien);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void delete(){
        try {
            repository.delete(alien);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }
}
