package com.yakimtsov.aliens.repository;

import com.yakimtsov.aliens.connection.ConnectionPool;
import com.yakimtsov.aliens.entity.Alien;
import com.yakimtsov.aliens.entity.User;
import com.yakimtsov.aliens.repository.alien.AlienRepository;
import com.yakimtsov.aliens.repository.user.UserRepository;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.fail;

public class UserRepositoryTest {
    UserRepository repository = new UserRepository();
    User user = new User();

    @BeforeClass
    public void setUp() {
        ConnectionPool.getInstance().initPool();
        user.setName("test");
        user.setPassword("test");
        user.setRole("user");
    }

    @AfterClass
    public void dispose() {
        ConnectionPool.getInstance().dispose();
    }

    @Test
    public void add(){
        try {
            repository.add(user);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void update(){
        try {
            user.setPassword("dsfdsfsdf");
            repository.update(user);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void delete(){
        try {
            repository.delete(user);
        } catch (RepositoryException e) {
            fail(e.getMessage());
        }
    }
}
